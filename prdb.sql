-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 17, 2022 at 09:22 PM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `prdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `authors`
--

CREATE TABLE `authors` (
  `author` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `authors`
--

INSERT INTO `authors` (`author`) VALUES
('Shawn');

-- --------------------------------------------------------

--
-- Table structure for table `date`
--

CREATE TABLE `date` (
  `inserteddate` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `date`
--

INSERT INTO `date` (`inserteddate`) VALUES
('2022-07-17');

-- --------------------------------------------------------

--
-- Table structure for table `descriptions`
--

CREATE TABLE `descriptions` (
  `description` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `descriptions`
--

INSERT INTO `descriptions` (`description`) VALUES
('The dish is normally prepared by marinating sirloin strips in vinegar, soy sauce and spices, and stir frying these with red onions, parsley, tomatoes, and possibly other ingredients.\r\nThe use of both potatoes (which originated in Peru) and rice (which originated in Asia) as starches are typical of the cultural blending that the dish represents.');

-- --------------------------------------------------------

--
-- Table structure for table `directions`
--

CREATE TABLE `directions` (
  `direction` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `directions`
--

INSERT INTO `directions` (`direction`) VALUES
(' Slice beef horizontally into 1/4-inch strips and transfer into a mixing bowl.'),
(' Add 1 tablespoon of soy sauce, and the sugar. '),
(' Mix well and place in the refrigerator for at least 1 hour; longer is fine.'),
(' Preheat an oven to 200 degrees F (90 degrees C). '),
(' Line a sheet pan with a silicone liner (such as Silpat®).'),
(' Heat 2 tablespoons olive oil in a large, nonstick skillet over medium-high heat. '),
(' Add French fries and cook until golden and crisp, 5 to 7 minutes.'),
(' Transfer to the prepared pan and place in the preheated oven to keep warm.'),
(' Heat 2 tablespoons olive oil in the same skillet over high heat until the oil begins to smoke. '),
(' Sear the beef strips in 2 or 3 smaller batches, until browned but still pink in places, for 1 to 2 minutes per batch.'),
(' As each batch of beef is browned, transfer to a plate or bowl, and reserve.'),
(' Place the skillet back over high heat, and add remaining 2 tablespoons olive oil. '),
(' Transfer in red onion, bell pepper, habanero, and green onion, along with a large pinch of salt. '),
(' Cook, stirring, until the onions and peppers just begin to soften, 3 to 5 minutes.'),
(' Add garlic and ginger to the pan; cook and stir for 1 minute.'),
(' Add tomato wedges and cook, stirring, until they begin to soften and start to release their juices, about 3 minutes. '),
(' Add remaining 2 tablespoons of soy sauce, white vinegar, and the browned beef into the pan, along with any accumulated juices, and stir to combine.'),
(' Stir cilantro and crispy, cooked French fries into the pan, tossing everything to combine. '),
('Taste for salt and adjust if needed. '),
('Serve immediately. ');

-- --------------------------------------------------------

--
-- Table structure for table `ingerdients`
--

CREATE TABLE `ingerdients` (
  `ingredient` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ingerdients`
--

INSERT INTO `ingerdients` (`ingredient`) VALUES
(' 1¼ pounds top sirloin steak'),
(' 3 tablespoons soy sauce, divided'),
(' 1 teaspoon white sugar'),
(' 1 (16 ounce) package frozen crinkle-cut French fries'),
(' 1½ cups sliced red onion'),
(' 1 medium orange bell pepper, sliced'),
(' 1 habanero pepper, seeded and minced'),
(' ½ cup sliced green onion'),
(' kosher salt to taste'),
(' 2 cloves garlic, minced '),
(' 1 teaspoon minced fresh ginger root'),
(' 4 medium roma (plum) tomato '),
(' ¼ cup white vinega '),
(' ¼ cup chopped fresh cilantro ');

-- --------------------------------------------------------

--
-- Table structure for table `mailpassword`
--

CREATE TABLE `mailpassword` (
  `msg` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mailpassword`
--

INSERT INTO `mailpassword` (`msg`) VALUES
('We have sent you an Email with a link to rest your password. Check your Email and click on the link in order to do that.');

-- --------------------------------------------------------

--
-- Table structure for table `mailvalid`
--

CREATE TABLE `mailvalid` (
  `msg` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mailvalid`
--

INSERT INTO `mailvalid` (`msg`) VALUES
('We have sent you an Email with a link to verify your Email. Check your Email and click on the link in order to do that');

-- --------------------------------------------------------

--
-- Table structure for table `recipes`
--

CREATE TABLE `recipes` (
  `writtenrecipe` mediumtext NOT NULL,
  `wpulicationdate` date NOT NULL,
  `wauthor` mediumtext NOT NULL,
  `sharedrecipe` mediumtext NOT NULL,
  `shpublicationdate` date NOT NULL,
  `shauthor` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `recipes`
--

INSERT INTO `recipes` (`writtenrecipe`, `wpulicationdate`, `wauthor`, `sharedrecipe`, `shpublicationdate`, `shauthor`) VALUES
('Lomo Saltado', '2022-01-01', 'Shawn', 'Pepperoni Pizza', '2022-01-01', 'Cody'),
('Eggplant Pasta', '2022-01-02', 'Shawn', 'Chili Nachos', '2022-01-02', 'Austin'),
('Caeser Salad', '2022-01-03', 'Shawn', 'Bronco Burger', '2022-01-03', 'Cody'),
('Chicken Taco', '2022-01-04', 'Shawn', 'Bohemian Orange Chhicken', '2022-01-04', 'Elie'),
('Smoked Salmon Sandwich', '2022-01-05', 'Shawn', 'Mac and Cheese', '2022-01-05', 'Austin');

-- --------------------------------------------------------

--
-- Table structure for table `restpassword`
--

CREATE TABLE `restpassword` (
  `msg` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `restpassword`
--

INSERT INTO `restpassword` (`msg`) VALUES
('Enter your Registered Email in order to send you a link to rest your password.');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
