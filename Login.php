<?php require_once("header.php");?>
<?php if(isset($_COOKIE["user_email"])) {
  $_SESSION["user_email"]=$_COOKIE["user_email"];
  header('Location: RecipesList.php');
  exit;
}
?>
<?php 
  if(isset($_POST['login_form'])) {
    require_once("users.php");
    if (isset($users[$_POST['user_email']])){
      if ($users[$_POST['user_email']] == $_POST['password'] ) {
        if ($_POST['remember_me']=="1") {
          echo "remeber me was checked";
          setcookie("user_email",$_POST['user_email'],time()+7*24*60*60);
        }
        if ($_POST['stay_logged']=="1") {
          echo "stay logged in was checked";
          setcookie("user_email",$_POST['user_email'],time()+7*24*60*60);
        }
        $_SESSION['user_email']=$_POST['user_email'];
        $_SESSION['password']=$_POST['password'];
        header('Location: RecipesList.php');
        exit;
      } else { ?>
      <div class="alert alert-danger" role="alert">
        Wrong Password
      </div>
      <?php
      }
    } else { ?>
      <div class="alert alert-warning" role="alert">
        Username not found
      </div>
    <?php
    }
  }
?>
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Login</title>
</head>
<body>
    <br><br>
    <main class="form-signin">
        <form method="POST">
            <div class="container">
                <div id="LoginScreen">
                    <h1 id="login">Login</h1>
                    <br>
                    <img id="avatar" src="avatar.jpg">
                    <br><br>
                    <div class="form-floating my-2">
                        <input name='user_email' type="text" class="form-control" id="floatingInput" placeholder="Enter Email">
                    </div>
                    <div class="form-floating my-2">
                        <input name='password' type="password" class="form-control" id="floatingPassword" placeholder="Enter Password">
                    </div>
                    <div class="checkbox mb-3">
                        <label style="color: white;">
                            <input type="checkbox" value="1" name="remember_me"> Remember me &nbsp; &nbsp; &nbsp; 
                        </label>
                        <label style="color: white;">
                            <input type="checkbox" value="1" name="stay_logged"> Stay Logged In
                          </label>
                    </div>
                    <p id="p1"><a href="ForgotPassword.php">Forgot Password</a></p>
                    <button id="b" name='login_form' class="w-100 btn btn-lg btn-primary" type="submit">Login</button>
                    <p id="p1">Don't have an account? <a href="Register.php">Register Now!</a></p>
                    <br><br>
                </div>  
            </div>     
        </form>
    </main>
    <br><br>
    <footer class="footer">© Copyright 2022. All Rights Reserved.</footer>
</body>
</html>