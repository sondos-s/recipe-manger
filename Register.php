<style><?php include 'style.css'; ?></style>
<?php echo'';?>
<?php require_once("header.php");?>
<?php
  require_once "DB.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://getbootstrap.com/docs/5.0/components/modal/">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Register</title>
</head>
<body>
    <br><br>
    <div class="container">
        <div id="RegisterScreen">
            <h1 id="login">Register</h1>
            <br>
            <img id="avatar" src="avatar.jpg">
            <br><br>
            <input type="email" placeholder="Enter Email" name="email" required>
            <br><br>
            <input type="password" placeholder="Enter Password" name="password" required>
            <br><br>
            <input type="password" placeholder="Confirm Password" name="confirmpassword" required>
            <br><br>
            <input type="text" placeholder="Choose a Username" name="username" required>
            <br><br>
            <button id="b" type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Submit</button>
        </div>
        <br><br><br><br><br>
    </div>
    </body>
    <footer class="footer">© Copyright 2022. All Rights Reserved.</footer>
</html>
    <!DOCTYPE html>
        <html lang="en">
        <head>
          <title>Bootstrap Example</title>
          <meta charset="utf-8">
          <meta name="viewport" content="width=device-width, initial-scale=1">
          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
          <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        </head>
        <body>
          <!-- Modal -->
          <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Mail Validation</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                    <div class="form-floating my-2">
                        <table>
                            <tbody>
                                <?php
                                    $sql = "SELECT * FROM mailvalid";
                                    $result = $conn->query ($sql);
                                ?>
                                <?php while($row = $result->fetch_assoc()):?>
                                <tr>
                                    <?=$row["msg"]?>
                                </tr>  
                                <?php endwhile;?>
                            </tbody>
                    </div>
                    <div class="modal-footer">
                        <button id="ok" type="button" class="btn btn-default" data-dismiss="modal">OK</button>
                    </div>  
                </div>
            </div>
          </div>
        </body>
        </html>