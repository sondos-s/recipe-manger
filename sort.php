<?php
    $con = mysqli_connect("localhost","root","","prodb");

    $sort_option = "";
    if(isset($_GET['sort_alphabet']))
    {
       if($_GET['sort_alphabet'] == "a-z")
       {
            $sort_option = "ASC";
        }
        elseif($_GET['sort_alphabet'] == "z-a")
        {
             $sort_option = "DESC";
        }
    }
                                        
    $query = "SELECT * FROM recipes ORDER BY writtenrecipe $sort_option";
    $query_run = mysqli_query($con, $query);

    if(mysqli_num_rows($query_run) > 0)
    {
        foreach($query_run as $row)
        {
?>
    <tr>
        <td><?= $row['writtenrecipe']; ?></td>
        <td><?= $row['wpublicationdate']; ?></td>
        <td><?= $row['wauthor']; ?></td>
        <td><?= $row['sharedrecipe']; ?></td>
        <td><?= $row['shpublicationdate']; ?></td>
        <td><?= $row['shauthor']; ?></td>
    </tr>
<?php
        }
    }
    else
    {
?>
        <tr>
            <td colspan="3">No Record Found</td>
        </tr>
<?php
    }
?>