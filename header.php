<?php
session_start();
require_once("users.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Recipe Book</title>
</head>
<body>
<div class="header">
<a class="" href=""><i class="fa fa-chevron-circle-left" style="font-size:36px;color:rgb(169, 9, 9)"></i></a>
        <a href=""><i class="fa fa-chevron-circle-right" style="font-size:36px;color:rgb(169, 9, 9)"></i></a>
        <a href="" class="logo"><img src="RBLogo.png" class="logo"></a>
        <a href="Register.php"><i class="fa fa-plus-square-o"style="font-size:16px;color:rgb(169, 9, 9)"></i>   Register</a>
        <a href="Logout.php"><i class="fa fa-plus-square-o"style="font-size:16px;color:rgb(169, 9, 9)"></i>   Logout</a>
        <a href="Login.php"><i class="fa fa-sign-in" style="font-size:16px;color:rgb(169, 9, 9)"></i>  Login</a>
        <a href="RecipesList.php"><i class="fa fa-book" style="font-size:16px;color:rgb(169, 9, 9)"></i>   RecipeList</a>
        <a href="Recipe.php"><i class="fa fa-file" style="font-size:16px;color:rgb(169, 9, 9)"></i>   Recipe</a>
    </div>
