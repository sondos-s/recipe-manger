<?php require_once("header.php");?>
<?php
  require_once "DB.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://getbootstrap.com/docs/5.0/components/modal/">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <title>Lomo Saltado</title>
</head>
<body>
    <div id="titleandpic">
        <br><br><br><br>
        <h1 id="lomotitle">Lomo Saltado</h1>
        <img src="LomoSaltado.jpg" class="LomoSaltado">
    </div>
    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;
    <div style="" id="da">
        <table id="ingredientstable">
                <tbody>
                  <?php
                    $sql = "SELECT * FROM date";
                    $result = $conn->query ($sql);
                  ?>
                  <?php while($row = $result->fetch_assoc()):?>
                    <tr>
                        <td>Inserted Date: <label for="cbox"><?=$row["inserteddate"]?></td>
                    </tr>
                    
                  <?php endwhile;?>
                </tbody>
                <tbody>
                  <?php
                    $sql = "SELECT * FROM authors";
                    $result = $conn->query ($sql);
                  ?>
                  <?php while($row = $result->fetch_assoc()):?>
                    <tr>
                        <td>Inserted By: <label for="cbox"><?=$row["author"]?></td>
                    </tr>
                    
                  <?php endwhile;?>
                </tbody>
        </table>
        </div>
        <div id="description">
        <table id="descriptiontable">
                <thead>
                    <tr>
                        <th id="descTitle" scope="col">Description</th>
                    </tr>
                </thead>
                <tbody>
                  <?php
                    $sql = "SELECT * FROM descriptions";
                    $result = $conn->query ($sql);
                  ?>
                  <?php while($row = $result->fetch_assoc()):?>
                    <tr>
                        <td><?=$row["description"]?></td>
                    </tr>
                    
                  <?php endwhile;?>
                </tbody>
                  </table>
                  <button ><a href=editDesc.php> Edit Description</a></button>
        </div>
        <br><br><br>
        <div style="" id="ingerdients">
        <table id="ingredientstable">
                <thead>
                    <tr>
                        <th id="ingredTitle" scope="col">Ingredients</th>
                    </tr>
                </thead>
                <tbody>
                  <?php
                    $sql = "SELECT * FROM ingerdients";
                    $result = $conn->query ($sql);
                  ?>
                  <?php while($row = $result->fetch_assoc()):?>
                    <tr>
                        <td><input type="checkbox" class="cbox" value="o_checkbox"> <label for="cbox"><?=$row["ingredient"]?></td>
                    </tr>
                    
                  <?php endwhile;?>
                </tbody>
                  </table>
                  <button ><a href=ingredTable.php> Edit Ingredients</a></button>
        </div>

        <br>
        <div style="" id="directions2">
        <table id="directionstable">
                <thead>
                    <tr>
                        <th id="directionsTitle" scope="col">Directions</th>
                    </tr>
                </thead>
                <tbody>
                  <?php
                    $sql = "SELECT * FROM directions";
                    $result = $conn->query ($sql);
                  ?>
                  <?php while($row = $result->fetch_assoc()):?>
                    <tr>
                        <td><input type="checkbox" class="cbox" value="o_checkbox"><?=$row["direction"]?></td>
                    </tr>
                  <?php endwhile;?>
                </tbody>
        </table>
        <button ><a href=editDirect.php> Edit Directions</a></button>
        </div>
        <br><br><br><br>
            <button id="managesharing" type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Manage Sharing</button>
        <br><br><br><br><br><br><br>
        <!DOCTYPE html>
        <html lang="en">
        <head>
          <meta charset="utf-8">
          <meta name="viewport" content="width=device-width, initial-scale=1">
          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
          <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        </head>
        <body>
          <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" style="text-align: left;">Share Recipe</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <input type="text" placeholder="Choose a Username" name="username" required>
                </div>
                <div class="modal-footer">
                    <button id="share" type="button" class="btn btn-default" data-dismiss="modal">Share</button>
                    <button id="cancel" type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
              </div>
            </div>
          </div>
        </body>
        </html>
</body>
    <footer class="footer">© Copyright 2022. All Rights Reserved.</footer>
</html>