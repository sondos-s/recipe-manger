<?php require_once("header.php");?>
<?php
  require_once "DB.php";
?>
<?php 
  if(isset($_POST['pass'])) {
    require_once("users.php");
    if (isset($users[$_POST['user_email']])){
    } else { ?>
      <div class="alert alert-warning" role="alert">
        Email not found
      </div>
    <?php
    }
  }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Login</title>
</head>
<body>
    <br><br><br><br><br>
    <main class="form-signin">
        <form method="POST">
            <div class="container">
            <div style="color: white; background: rgba(0, 0, 0, 0.5);">
                    <h3 id="pass">  &nbsp; &nbsp; Reset Password</h3>
                    <br>
                    &nbsp; &nbsp;
                        <?php
                          $sql = "SELECT * FROM restpassword";
                          $result = $conn->query ($sql);
                          ?>
                          <?php while($row = $result->fetch_assoc()):?>
                            <tr>
                                <td><?=$row["msg"]?></td>
                            <tr>
                  <?php endwhile;?>
                </tbody>
                  </table>
                  <br><br>
                        <input style="width: 400px" name='user_email' type="text" class="form-control" id="floatingInput" placeholder="Enter Your Email">
                        <br>
                        <button name='pass' class="w-100 btn btn-lg btn-primary" type="submit" style=" text-align: center;background-color: rgb(169, 9, 9); color: white; width: 10px;">Send Link</button>
            </div>     
        </form>
    </main>
    <br><br>
    <footer class="footer">© Copyright 2022. All Rights Reserved.</footer>
</body>
</html>
