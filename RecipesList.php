<?php require_once("header.php");?>
<?php
  require_once "DB.php";
?>
<?php 
  if(!isset($_SESSION['user_email'])) {
      if(!isset($_SESSION['password'])) {
          header('Location: Login.php');
        exit;
      }
  }
  ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Recipe List</title>
</head>
<body>
    <div class="container">
        <div id="RecipeList">
        <br><br><br><br>
        <h1 id="recipelisttitle">Recipe List</h1>
        <br>
        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
        <label for="gsearch" style="color: white">Search In Written Recipes:</label>
        <input type="text" id="myInput1" onkeyup="myFunction()" placeholder="Search Recipe By Name">
        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
        <label for="gsearch" style="color: white">Search In Shared Recipes :</label>
        <input type="text" id="myInput2" onkeyup="myFunction2()" placeholder="Search Recipe By Name">
        <br><br>
            <table id="myTable1">
                <thead>
                    <tr>
                    <tr>
                        <th scope="col"><a href="javascript:SortTable(0,'T');">Written Recipe</a></th>
                        <th scope="col"><a href="javascript:SortTable(1,'D','dmy');">Publication Date</a></th>
                        <th scope="col">Author</th>
                        <th scope="col"><a href="javascript:SortTable(3,'T');">Shared Recipe</a></th>
                        <th scope="col"><a href="javascript:SortTable(4,'D','dmy');">Publication Date</a></th>
                        <th scope="col">Author</th>
                </thead>
                <tbody>
                  <?php
                    $sql = "SELECT * FROM recipes";
                    $result = $conn->query ($sql);
                  ?>
                  <?php while($row = $result->fetch_assoc()):?>
                    <tr>
                        <td><?=$row["writtenrecipe"]?></td>
                        <td><?=$row["wpulicationdate"]?></td>
                        <td><?=$row["wauthor"]?></td>
                        <td><?=$row["sharedrecipe"]?></td>
                        <td><?=$row["shpublicationdate"]?></td>
                        <td><?=$row["shauthor"]?></td>
                    </tr>
                  <?php endwhile;?>
                </tbody>
                
            </table>
        </div>
      </div>
      <br><br><br><br><br><br><br>
<script type="text/javascript"> 
var TableIDvalue = "myTable1"; 
 
var TableLastSortedColumn = -1; 
function SortTable() { 
var sortColumn = parseInt(arguments[0]); 
var type = arguments.length > 1 ? arguments[1] : 'T'; 
var dateformat = arguments.length > 2 ? arguments[2] : ''; 
var table = document.getElementById(TableIDvalue); 
var tbody = table.getElementsByTagName("tbody")[0]; 
var rows = tbody.getElementsByTagName("tr"); 
var arrayOfRows = new Array(); 
type = type.toUpperCase(); 
dateformat = dateformat.toLowerCase(); 
for(var i=0, len=rows.length; i<len; i++) { 
 arrayOfRows[i] = new Object; 
 arrayOfRows[i].oldIndex = i; 
 var celltext = rows[i].getElementsByTagName("td")[sortColumn].innerHTML.replace(/<[^>]*>/g,""); 
 if( type=='D' ) { arrayOfRows[i].value = GetDateSortingKey(dateformat,celltext); } 
 else { 
  var re = type=="N" ? /[^\.\-\+\d]/g : /[^a-zA-Z0-9]/g; 
  arrayOfRows[i].value = celltext.replace(re,"").substr(0,25).toLowerCase(); 
  } 
 } 
if (sortColumn == TableLastSortedColumn) { arrayOfRows.reverse(); } 
else { 
 TableLastSortedColumn = sortColumn; 
 switch(type) { 
  case "N" : arrayOfRows.sort(CompareRowOfNumbers); break; 
  case "D" : arrayOfRows.sort(CompareRowOfNumbers); break; 
  default : arrayOfRows.sort(CompareRowOfText); 
  } 
 } 
var newTableBody =
 
document.createElement("tbody"); 
for(var i=0, len=arrayOfRows.length; i<len; i++) { 
 newTableBody.appendChild(rows[arrayOfRows[i].oldIndex].cloneNode(true)); 
 } 
table.replaceChild(newTableBody,tbody); 
} // function SortTable() 
 
function CompareRowOfText(a,b) { 
var aval = a.value; 
var bval = b.value; 
return( aval == bval ? 0 : (aval > bval ? 1 : -1) ); 
} // function CompareRowOfText() 
 
function CompareRowOfNumbers(a,b) { 
var aval = /\d/.test(a.value) ? parseFloat(a.value) : 0; 
var bval = /\d/.test(b.value) ? parseFloat(b.value) : 0; 
return( aval == bval ? 0 : (aval > bval ? 1 : -1) ); 
} // function CompareRowOfNumbers() 
 
function GetDateSortingKey(format,text) { 
if( format.length < 1 ) { return ""; } 
format = format.toLowerCase(); 
text = text.toLowerCase(); 
text = text.replace(/^[^a-z0-9]*/,""); 
text = text.replace(/[^a-z0-9]*$/,""); 
if( text.length < 1 ) { return ""; } 
text = text.replace(/[^a-z0-9]+/g,","); 
var date = text.split(","); 
if( date.length < 3 ) { return ""; } 
var d=0, m=0, y=0; 
for( var i=0; i<3; i++ ) { 
 var ts = format.substr(i,1); 
 if( ts == "d" ) { d = date[i]; } 
 else if( ts == "m" ) { m = date[i]; } 
 else if( ts == "y" ) { y = date[i]; } 
 } 
d = d.replace(/^0/,""); 
if( d < 10 ) { d = "0" + d; } 
if( /[a-z]/.test(m) ) { 
 m = m.substr(0,3); 
 switch(m) { 
  case "jan" : m = String(1); break; 
  case "feb" : m = String(2); break; 
  case "mar" : m = String(3); break; 
  case "apr" : m = String(4); break; 
  case "may" : m = String(5); break; 
  case "jun" : m = String(6); break; 
  case "jul" : m = String(7); break; 
  case "aug" : m = String(8); break; 
  case "sep" : m = String(9); break; 
  case "oct" : m = String(10); break; 
  case "nov" : m = String(11); break; 
  case "dec" : m = String(12); break; 
  default : m = String(0); 
  } 
 } 
m = m.replace(/^0/,""); 
if( m < 10 ) { m = "0" + m; } 
y = parseInt(y); 
if( y < 100 ) { y = parseInt(y) + 2000; } 
return "" + String(y) + "" + String(m) + "" + String(d) + ""; 
} 
</script> 
<footer class="footer">© Copyright 2022. All Rights Reserved.</footer>
</body>
<script>
myFunction = function() {
  var input, filter, table, tr, td, i, txtValue, index;
  input = document.getElementById("myInput1");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable1");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[0];
    if (td) {
      txtValue = td.textContent || td.innerText;
      td.innerHTML = txtValue;
      index = txtValue.toUpperCase().indexOf(filter);
      if (index > -1) {
        td.innerHTML = txtValue.substring(0, index) + "<mark>" + txtValue.substring(index, index + filter.length) + "</mark>" + txtValue.substring(index + filter.length);
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}
</script>
<script>
myFunction2 = function() {
  var input, filter, table, tr, td, i, txtValue, index;
  input = document.getElementById("myInput2");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable1");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[3];
    if (td) {
      txtValue = td.textContent || td.innerText;
      td.innerHTML = txtValue;

      index = txtValue.toUpperCase().indexOf(filter);
      if (index > -1) {
        td.innerHTML = txtValue.substring(0, index) + "<mark>" + txtValue.substring(index, index + filter.length) + "</mark>" + txtValue.substring(index + filter.length);
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}
</script>
</html>